import 'package:flutter/material.dart';
import 'package:food_d/utils/Colors/colors.dart';

class FoodPageBody extends StatefulWidget {
  const FoodPageBody({super.key});

  @override
  State<FoodPageBody> createState() => _FoodPageBodyState();
}

class _FoodPageBodyState extends State<FoodPageBody> {
  @override
  Widget build(BuildContext context) {
    PageController pageController = PageController();
    return Container(
        height: 320,
        child: PageView.builder(
          itemCount: 5,
          itemBuilder: ((context, index) {
            return Container(
              height: 320,
              color: AppColors.mainBlackColor,
              decoration: BoxDecoration(),
            );
          }),
        ));
  }
}
