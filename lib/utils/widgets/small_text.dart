import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:google_fonts/google_fonts.dart';

class SmallText extends StatelessWidget {
  String text;
  double size;
  TextOverflow overflow;
  Color color;
  double height;
  SmallText(
      {super.key,
      this.color = const Color(0xFFccc7c5),
      this.overflow = TextOverflow.ellipsis,
      this.size = 12,
      this.height = 1.2,
      required this.text});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: GoogleFonts.roboto(
          textStyle: TextStyle(
              color: color,
              fontSize: size,
              overflow: overflow,
              height: height)),
    );
  }
}
