import 'package:flutter/cupertino.dart';

import 'package:food_d/utils/Colors/colors.dart';
import 'package:google_fonts/google_fonts.dart';

class BigText extends StatelessWidget {
  String text;
  double Size;
  Color? color;
  TextOverflow overFlow;
  BigText(
      {super.key,
      this.color = const Color(0xFF332d2b),
      this.Size = 20,
      this.overFlow = TextOverflow.ellipsis,
      required this.text});

  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: GoogleFonts.roboto(
          textStyle: TextStyle(
            fontWeight: FontWeight.w400,
            fontSize: Size,
            color: color,
            overflow: overFlow,
          ),
        ));
  }
}
